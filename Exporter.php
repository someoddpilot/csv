<?php

namespace someoddpilot\csv;

class Exporter
{
  public function export($type, array $metaQuery, $headers, $trasformCallback)
  {
    if (!current_user_can('manage_options')) {
      return;
    }

    $posts = $this->query($type, $metaQuery, $trasformCallback);

    $file = $this->openFile();

    $this->exportCsv($file, $headers, $posts);
  }

  private function query($type, array $metaQuery, $trasformCallback)
  {
    $posts = get_posts(
      array(
        'posts_per_page' => -1,
        'post_type' => 'purchase',
        'meta_query' => array_merge(
          array(
            array(
              'key'   => 'type',
              'value' => $type,
            )
          ),
          $metaQuery
        )
      )
    );

    return array_map(
      $trasformCallback,
      $posts
    );
  }

  private function openFile()
  {
    return fopen('php://output', 'w');
  }

  private function exportCsv($filepointer, array $headers, array $data)
  {
    var_dump($data);
    exit;
    header("Content-Type:application/csv");

    $filename = "filename=export-" . date('Y-m-d-H-i-s') .".csv";
    header("Content-Disposition:attachment;" . $filename);

    fputcsv($filepointer, $headers);
    foreach ($data as $row) {
      fputcsv($filepointer, $row);
    }
  }
}
