<?php
/**
 * Plugin Name: SOP CSV
 */

namespace someoddpilot\csv;

add_filter('wp_ajax_sop_export_donation', function () {
  $exporter = new Exporter();

  $exporter->export(
    'Donation',
    array(),
    array(
      'type',
      'date',
      'user',
      'amount',
    ),
    function ($post) {
      $author = get_userdata($post->post_author);

      $params = get_post_meta($post->ID, 'params', true);

      return array(
        $post->post_title,
        $post->post_date,
        $author->data->user_nicename,
        $params['amount']
      );
    }
  );

  exit;
});

add_filter('wp_ajax_sop_export_membership', function () {
  $exporter = new Exporter();

  $exporter->export(
    'Membership',
    array(),
    array(
      'type',
      'date',
      'user',
      'level',
    ),
    function ($post) {
      $author = get_userdata($post->post_author);

      $params = get_post_meta($post->ID, 'params', true);

      return array(
        $post->post_title,
        $post->post_date,
        $author->data->user_nicename,
        $params['level'],
      );
    }
  );

  exit;
});

add_filter('wp_ajax_sop_export_event', function () {
  $exporter = new Exporter();

  $exporter->export(
    'Event',
    array(),
    array(
      'type',
      'date',
      'user',
      'level',
      'event',
      'option',
    ),
    function ($post) {
      $author = get_userdata($post->post_author);

      $params = get_post_meta($post->ID, 'params', true);

      $pricing = get_field('pricing', $params['id']);

      return array(
        $post->post_title,
        $post->post_date,
        $author->data->user_nicename,
        $author->roles[0],
        get_the_title($params['id']),
        $pricing[$params['option']]['option_name'],
      );
    }
  );

  exit;
});

